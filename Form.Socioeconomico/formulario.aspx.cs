﻿
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Form.Socioeconomico
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TbNome_TextChanged(object sender, EventArgs e)
        {

        }

        protected void RbQuestao02_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        protected void TbTelefone_TextChanged(object sender, EventArgs e)
        {

        }

        protected void BtProxima_Click(object sender, EventArgs e)
        {

        }

        protected void BtEnviar_Click(object sender, EventArgs e)
        {
            var connString = "Server=localhost;Database=db_formulario;Uid=root;Password=root";
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();

            try
            {
                connection.Open();
                if (Rfnome.IsValid && RfEndereco.IsValid && RfBairro.IsValid && RevEmail.IsValid && CvDatanascimento.IsValid && RfQuestao1.IsValid && RfQuestao2.IsValid && RfQuestao3.IsValid && RfQuestao4.IsValid && RfQuestao5.IsValid && RfQuestao6.IsValid)
                {
                    command.CommandText = "INSERT INTO tb_form (nome, telefone, endereco,bairro,email,nascimento, qtd_pessoas , salario,escolaridade,moradia,estado_civil,cor_raca,itens) VALUES ('" + TbNome.Text + "' , '" + TbTelefone.Text + "' , '" + TbEndereco.Text + "' , '" + TbBairro.Text + "' ,'" + TbEmail.Text + "' ,'" + TbDtnascimento.Text + "' ,'" + TbQuestao1.Text + "' , '" + RbQuestao02.SelectedValue + "' , '" + DdQuestao3.SelectedValue + "' , '" + RbQuestao4.SelectedValue + "' , '" + DdQuestao5.SelectedValue + "' , '" + RbQuestao6.SelectedValue + "', '" + CbQuestao7.SelectedValue + "')";
                    command.ExecuteNonQuery();
                    alerta_lbl.Text = "Dados enviados!";
                }
            }

            catch (Exception ex)
            {
                alerta_lbl.Text = "Não foi possível conectar com o banco de dados. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void CvDatanascimento_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (DateTime.Parse(args.Value) > DateTime.Now)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void CbQuestao7_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void TbDtnascimento_TextChanged(object sender, EventArgs e)
        {

            DateTime dt = DateTime.Parse("01/01/2000");
            string d = dt.ToString("yyyy-MM-dd");

        }

        protected void BtListar_Click(object sender, EventArgs e)
        {
            var connString = "Server=localhost;Database=db_formulario;Uid=root;Password=root";
            var connection = new MySqlConnection(connString);
            var ds = new DataSet();

            try
            {
                connection.Open();
                MySqlDataAdapter conexaoAdapter = new MySqlDataAdapter("Select nome as Nome, telefone as Telefone, endereco as Endereco , bairro as Bairro, email as 'E-mail', nascimento as 'Data de nascimento' from tb_form", connection);
                conexaoAdapter.Fill(ds);
                GridView.DataSource = ds;
                GridView.DataBind();
                GridView.RowStyle.HorizontalAlign = HorizontalAlign.Center;
                GridView.HeaderStyle.ForeColor = Color.Blue;
            }
            catch (Exception ex)
            {
                alerta_lbl.Text = "Não foi possível conectar com o banco de dados. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();

            }


        }

        protected void TbNovotelefone_TextChanged(object sender, EventArgs e)
        {

        }

        protected void BtAtualizar_Click(object sender, EventArgs e)
        {
            var connString = "Server=localhost;Database=db_formulario;Uid=root;Password=root";
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();

            try
            {

                connection.Open();
                if (TbPesquisanome.Text.Length > 0)
                {
                    command.CommandText = "Update tb_form set email = '" + TbNovoemail.Text + "' , telefone = '" + TbNovotelefone.Text + "' where nome like '" + TbPesquisanome.Text + "' ";
                    int rowAffected = command.ExecuteNonQuery();

                    if (rowAffected > 0)
                    {
                        LbAlerta2.Text = "Dados atualizado com sucesso!";
                    }
                    else
                    {
                        LbAlerta2.Text = "Os dados não foram atualizados, verifique o nome de busca!";
                    }
                }

            }
            catch (Exception ex)
            {
                LbAlerta2.Text = "Não foi possível conectar com o banco de dados. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();

            }


        }

        protected void BtApagar_Click(object sender, EventArgs e)
        {
            var connString = "Server=localhost;Database=db_formulario;Uid=root;Password=root";
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();

            try
            {
                connection.Open();
                if (!TbNome.Text.Equals(null) && !TbNome.Text.Equals(""))
                {
                    command.CommandText = " Delete from tb_form where nome = '" + TbNome.Text + "' ";
                    command.ExecuteNonQuery();
                    alerta_lbl.Text = " Dados deletados!!";

                }
                else if (!TbTelefone.Text.Equals(null) && !TbTelefone.Text.Equals(""))
                {
                    command.CommandText = " Delete from tb_form where telefone = '" + TbTelefone.Text + "' ";
                    command.ExecuteNonQuery();
                    alerta_lbl.Text = " Dados deletados!!";
                }
                else
                {
                    alerta_lbl.Text = "Não há registro para ser apagadado :(";
                }
            }
            catch (Exception ex)
            {
                alerta_lbl.Text = "Não foi possível conectar com o banco de dados. \n" + ex.Message;
            }

            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();

            }


        }

    }
            
        

    
}
