﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="formulario.aspx.cs" Inherits="Form.Socioeconomico.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #form1 {
            height: 1823px;
            width: 799px;
        }
    </style>
</head>
<body style="height: 1825px; width: 810px">
    <form id="form1" runat="server">
        <div style="background-color: #49CFFE; position: inherit; width: auto;">
            <asp:Label ID="Lbtitulo" runat="server" BackColor="#33CCFF" Font-Bold="True" Font-Names="Arial" Font-Size="Larger" Font-Strikeout="False" Text="Formulário Socioeconômico"></asp:Label>
        </div>
        <p>
            <asp:Label ID="LbNome" runat="server" Font-Bold="True" Font-Names="Arial" Text="Nome:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TbNome" runat="server" CausesValidation="True" Font-Names="Arial" Height="16px" OnTextChanged="TbNome_TextChanged" Width="270px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Rfnome" runat="server" ControlToValidate="TbNome" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="LbTelefone" runat="server" Font-Bold="True" Font-Names="Arial" Text="Telefone:"></asp:Label>
&nbsp;&nbsp;
            <asp:TextBox ID="TbTelefone" runat="server" Width="264px" MaxLength="11" OnTextChanged="TbTelefone_TextChanged" 
                ></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="LbEndereco" runat="server" Font-Bold="True" Font-Names="Arial" Text="Endereço:"></asp:Label>
&nbsp;<asp:TextBox ID="TbEndereco" runat="server" CausesValidation="True" Width="258px"></asp:TextBox>
&nbsp;
            <asp:RequiredFieldValidator ID="RfEndereco" runat="server" ControlToValidate="TbEndereco" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
&nbsp;<asp:Label ID="LbBairro" runat="server" ClientIDMode="AutoID" Font-Bold="True" Font-Names="Arial" Text="Bairro:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TbBairro" runat="server" Width="157px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RfBairro" runat="server" ControlToValidate="TbBairro" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="LbEmail" runat="server" Font-Bold="True" Font-Names="Arial" Text="E-mail:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TbEmail" runat="server" Width="258px" CausesValidation="True"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RevEmail" runat="server" ControlToValidate="TbEmail" EnableClientScript="False" ErrorMessage="*e-amail inválido" Font-Size="Small" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </p>
        <p>
            <asp:Label ID="LbDtnascimento" runat="server" Font-Bold="True" Font-Names="Arial" Text="Data de nascimento:"></asp:Label>
&nbsp;<asp:TextBox ID="TbDtnascimento" runat="server" Width="183px" CausesValidation="True" OnTextChanged="TbDtnascimento_TextChanged"></asp:TextBox>
            <asp:CustomValidator ID="CvDatanascimento" runat="server" ControlToValidate="TbDtnascimento" EnableClientScript="False" ErrorMessage="*data inválida" Font-Size="Small" ForeColor="#FF3300" OnServerValidate="CvDatanascimento_ServerValidate"></asp:CustomValidator>
        </p>
        <p>
            <asp:Label ID="LbQuestao1" runat="server" Font-Bold="True" Font-Names="Arial" Text="Quantas pessoas reside na sua casa:"></asp:Label>
&nbsp;<asp:TextBox ID="TbQuestao1" runat="server" Width="56px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RfQuestao1" runat="server" ControlToValidate="TbQuestao1" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="LbQuestao2" runat="server" Font-Bold="True" Font-Names="Arial" Text="Qual a média salarial da sua familia:"></asp:Label>
        &nbsp;</p>
        <asp:RadioButtonList ID="RbQuestao02" runat="server" Font-Names="Arial" OnSelectedIndexChanged="RbQuestao02_SelectedIndexChanged" CausesValidation="True">
            <asp:ListItem>De 0 a 1.100</asp:ListItem>
            <asp:ListItem>De 1.101 a 2.500</asp:ListItem>
            <asp:ListItem>De 2.501 a 5.000</asp:ListItem>
            <asp:ListItem>Mais de 5.001</asp:ListItem>
        </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="RfQuestao2" runat="server" ControlToValidate="RbQuestao02" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        &nbsp;<p>
            <asp:Label ID="LbQuestao3" runat="server" Font-Bold="True" Font-Names="Arial" Text="Qual a sua escolaridade:"></asp:Label>
&nbsp;<asp:DropDownList ID="DdQuestao3" runat="server" Font-Names="Arial" CausesValidation="True">
                <asp:ListItem>Selecione...</asp:ListItem>
                <asp:ListItem>1° Grau Completo</asp:ListItem>
                <asp:ListItem>1° Grau incompleto</asp:ListItem>
                <asp:ListItem>2° Grau completo</asp:ListItem>
                <asp:ListItem>2° Grau incompleto</asp:ListItem>
                <asp:ListItem>Superior Completo</asp:ListItem>
                <asp:ListItem>Superiro incompleto</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RfQuestao3" runat="server" ControlToValidate="DdQuestao3" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="LbQuestao4" runat="server" Font-Bold="True" Font-Names="Arial" Text="Situação da moradia:"></asp:Label>
        </p>
        <asp:RadioButtonList ID="RbQuestao4" runat="server" Font-Names="Arial">
            <asp:ListItem>Próprio</asp:ListItem>
            <asp:ListItem>Alugado</asp:ListItem>
            <asp:ListItem>Financiado</asp:ListItem>
        </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="RfQuestao4" runat="server" ControlToValidate="RbQuestao4" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        <p>
            <asp:Label ID="LbQuestao5" runat="server" Font-Bold="True" Font-Names="Arial" Text="Estado civil:"></asp:Label>
&nbsp;<asp:DropDownList ID="DdQuestao5" runat="server" Font-Names="Arial" CausesValidation="True">
                <asp:ListItem>Selecione...</asp:ListItem>
                <asp:ListItem>Casado(a)</asp:ListItem>
                <asp:ListItem>Amasiado(a)</asp:ListItem>
                <asp:ListItem>Solteiro(a)</asp:ListItem>
                <asp:ListItem>Divorciado(a)</asp:ListItem>
                <asp:ListItem>Viuvo(a)</asp:ListItem>
            </asp:DropDownList>
        &nbsp;<asp:RequiredFieldValidator ID="RfQuestao5" runat="server" ControlToValidate="DdQuestao5" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <p>
            <asp:Label ID="LbQuestao6" runat="server" Font-Bold="True" Font-Names="Arial" Text="De acordo com as categorias de cor/raça do IBGE, você se considera:"></asp:Label>
        </p>
        <asp:RadioButtonList ID="RbQuestao6" runat="server" Font-Names="Arial">
            <asp:ListItem>Branco</asp:ListItem>
            <asp:ListItem>Preto</asp:ListItem>
            <asp:ListItem>Amarelo (de origem asiática)</asp:ListItem>
            <asp:ListItem>Pardo</asp:ListItem>
            <asp:ListItem>Indígena</asp:ListItem>
        </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="RfQuestao6" runat="server" ControlToValidate="RbQuestao6" EnableClientScript="False" ErrorMessage="* campo obrigartório" Font-Size="Small" ForeColor="Red"></asp:RequiredFieldValidator>
        <p>
            <asp:Label ID="LbQuestao7" runat="server" Font-Bold="True" Font-Names="Arial" Text="Escolha dos itens abaixo o que você possui em sua casa:"></asp:Label>
        </p>
        <asp:CheckBoxList ID="CbQuestao7" runat="server" Font-Names="Arial" CausesValidation="True" Height="252px" Width="178px" OnSelectedIndexChanged="CbQuestao7_SelectedIndexChanged">
            <asp:ListItem>Televisor</asp:ListItem>
            <asp:ListItem>Computador de mesa</asp:ListItem>
            <asp:ListItem>Smathphone</asp:ListItem>
            <asp:ListItem>Lavadora de roupas</asp:ListItem>
            <asp:ListItem>Secadora de roupas</asp:ListItem>
            <asp:ListItem>Video Game</asp:ListItem>
            <asp:ListItem>Lavadora de pratos</asp:ListItem>
            <asp:ListItem>Microondas</asp:ListItem>
            <asp:ListItem>Forno elétrico</asp:ListItem>
            <asp:ListItem>Notebook</asp:ListItem>
        </asp:CheckBoxList>
        <br />
        <br />
        <asp:Label ID="alerta_lbl" runat="server" BackColor="White" Font-Bold="True" Font-Names="Arial"></asp:Label>
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="BtEnviar" runat="server" Height="31px" Text="Enviar" Width="66px" OnClick="BtEnviar_Click" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
    &nbsp;&nbsp;&nbsp;
        <asp:Button ID="BtApagar" runat="server" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Height="31px" OnClick="BtApagar_Click" style="margin-right: 2px" Text="Apagar" Width="66px" />
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="BtListar" runat="server" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Height="31px" OnClick="BtListar_Click" Text="Listar" Width="66px" />
        <p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:GridView ID="GridView" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="Medium" ForeColor="Black">
        </asp:GridView>
        <br />
        <asp:Label ID="LbPesquisa" runat="server" Font-Bold="True" Font-Names="Arial" Text="Buscar por: "></asp:Label>
        <asp:TextBox ID="TbPesquisanome" runat="server" Font-Names="Arial" Height="16px" Width="239px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="LbPesquisa2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" Text="Novo e-mail:"></asp:Label>
&nbsp;
        <asp:TextBox ID="TbNovoemail" runat="server" Width="232px"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="LbPesquisa3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" Text="Novo telefone:"></asp:Label>
&nbsp;<asp:TextBox ID="TbNovotelefone" runat="server" MaxLength="11" OnTextChanged="TbNovotelefone_TextChanged" Width="216px"></asp:TextBox>
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="BtAtualizar" runat="server" BorderStyle="None" Font-Bold="True" Height="30px" OnClick="BtAtualizar_Click" Text="Atualizar" Width="85px" />
        <br />
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="LbAlerta2" runat="server" Font-Bold="True" Font-Names="Arial"></asp:Label>
        <br />
        <br />
        <br />
        <br />
    </form>
</body>
</html>
